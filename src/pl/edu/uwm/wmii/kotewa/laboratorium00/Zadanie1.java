package pl.edu.uwm.wmii.kotewa.laboratorium00;

public class Zadanie1 {

    public static void zad2() {
        int suma = 0;
        for(int i=1; i<=10; i++) {
            suma += i;
        }
        System.out.println(suma);
    }

    public static void zad3() {
        int suma = 1;
        for(int i=1; i<=10; i++) {
            suma *= i;
        }
        System.out.println(suma);
    }

    public static void zad4() {
        int saldo = 1000;
        double procent = 0.06;
        double rok1 = saldo + saldo * procent;
        double rok2 = rok1 + rok1 * procent;
        double rok3 = rok2 + rok2 * procent;
        System.out.printf(" %.2f%n %.2f%n %.2f%n", rok1, rok2, rok3);

    }

    public static void zad5() {
        System.out.println(" ________\n | Java |\n --------");
    }

    public static void zad6() {
        System.out.println("   ////\n +'''''+\n(| o o |)\n  | ^ |\n | '_' |\n +_____+ ");
    }

    public static void zad8() {
        System.out.println("   +\n  + +  \n +   + \n+-----+\n| .-. |\n| | | |\n+-+-+-+");
    }
    public static void main(String[] args) {
	    System.out.println(31+29+31);
        zad2();
        zad3();
        zad4();
        zad5();
        zad6();
        zad8();
    }
}
